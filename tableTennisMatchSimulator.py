#Table tennis game simulator, Xavier de Blas, License: GPL
#xaviblas@gmail.com
#Dec 2023

#TODO:
#show the point winner of each set in yellow (the 11 or greater)
#get the list of matches from the internet (at the moment from file seguent.txt)

#execute from the terminal or here:
#https://trinket.io/python3/294739ca4b

#to pause the program on a Linux system use ctrl+z and then fg for continue

import sys # for exit
import os, random, time
from termcolor import colored #https://pypi.org/project/termcolor/
clear = lambda: os.system('clear') #to clear screen on trinket using clear ()

#p_l és la llista de partits
p_l = [
        {
            'grup':'',
            'jugA':'',
            'jugB':'',
            'puntsA':[0, 0, 0, 0, 0],
            'puntsB':[0, 0, 0, 0, 0],
            'joc':0,
            'partitAcabat':False,
            'guanyador':'',
            'serveiIniciJoc':'A',
            'serveiCount':'0',
            'jugAPuntBollo':False,
            'jugBPuntBollo':False,
            'jugABollos':0,
            'jugBBollos':0,
            'jugACeleb':"",
            'jugBCeleb':"",
            'tempsMortA': -1, # -1: no demanat; 0-4: els punts d'espera; 5-8 els punts on té ventatja; 9: s'acabat la ventatja
            'tempsMortB':-1,
            'connectatA':False,
            'connectatB':False,
            },
        {
            'grup':'',
            'jugA':'',
            'jugB':'',
            'puntsA':[0, 0, 0, 0, 0],
            'puntsB':[0, 0, 0, 0, 0],
            'joc':0,
            'partitAcabat':False,
            'guanyador':'',
            'serveiIniciJoc':'A',
            'serveiCount':'0',
            'jugAPuntBollo':False,
            'jugBPuntBollo':False,
            'jugABollos':0,
            'jugBBollos':0,
            'jugACeleb':"",
            'jugBCeleb':"",
            'tempsMortA':-1,
            'tempsMortB':-1,
            'connectatA':False,
            'connectatB':False,
            },
        {
            'grup':'',
            'jugA':'',
            'jugB':'',
            'puntsA':[0, 0, 0, 0, 0],
            'puntsB':[0, 0, 0, 0, 0],
            'joc':0,
            'partitAcabat':False,
            'guanyador':'',
            'serveiIniciJoc':'A',
            'serveiCount':'0',
            'jugAPuntBollo':False,
            'jugBPuntBollo':False,
            'jugABollos':0,
            'jugBBollos':0,
            'jugACeleb':"",
            'jugBCeleb':"",
            'tempsMortA':-1,
            'tempsMortB':-1,
            'connectatA':False,
            'connectatB':False,
            },
        {
            'grup':'',
            'jugA':'',
            'jugB':'',
            'puntsA':[0, 0, 0, 0, 0],
            'puntsB':[0, 0, 0, 0, 0],
            'joc':0,
            'partitAcabat':False,
            'guanyador':'',
            'serveiIniciJoc':'A',
            'serveiCount':'0',
            'jugAPuntBollo':False,
            'jugBPuntBollo':False,
            'jugABollos':0,
            'jugBBollos':0,
            'jugACeleb':"",
            'jugBCeleb':"",
            'tempsMortA':-1,
            'tempsMortB':-1,
            'connectatA':False,
            'connectatB':False,
            }
        ]
tempsPerPunt = 2
#tempsPerPunt = .1 #per fer proves


def serveiPerA ():
    return bool(random.getrandbits(1))

def puntPerA (serveiA, boostA, boostB, connectatA, connectatB):
    dau = random.randint(1,100)
    if serveiA:
        dau = dau +10
    else:
        dau = dau -10

    if boostA:
        dau = dau +20
    if boostB:
        dau = dau -20

    if connectatA:
        dau = dau +4
    if connectatB:
        dau = dau -4

    return dau > 50

def puntDeBollo ():
    dau = random.randint(1,100)
    if dau >= 97:
        return True
    else:
        return False

def puntDeCelebracio ():
    c_l = ["Chuuu", "Vinga", "Vamos", "yes!", "Super!", "Toma!", "Som-hi!",
            "Vinga va!", "Cho le", "Que se joda!", "Ho graves Solsona?",
            "La pachanga nos libera!", "Bailarrrr", "Serpi", "Un sinmi",
            "Topspin panenka", "Soy leyenda",
            "Falta fondo...", "Ets un moooort"]

    dau = random.randint(1,100)
    if dau >= 75:
        return c_l[random.randint(0, len(c_l) -1)]
    else:
        return ""

def demanaTempsMort (joc, puntsJugadorQueDemanaria, puntsAltre):
    if joc >= 3 and puntsJugadorQueDemanaria[joc] +3 <= puntsAltre[joc]:
        return random.randint(1,100) > 75
    else:
        return False

def tempsMortEspera (tempsMort): #seguir amb el temps que sigui, per a que es pugui cridar des de imprimeix, i el tempsMortEspera dels dos, que cridi aquí
    if tempsMort >= 0 and tempsMort <= 4:
        return True
    else:
        return False

def tempsMortEspera2 (tempsMortA, tempsMortB):
    if tempsMortEspera (tempsMortA):
        return True
    if tempsMortEspera (tempsMortB):
        return True
    return False

def tempsMortBoost (tempsMort): #els 4 punts en que té ventatja després del temps mort
    if tempsMort >= 5 and tempsMort <= 8:
        return True
    else:
        return False

def tempsMortBoost2 (tempsMortA, tempsMortB): #els 4 punts en que té ventatja després del temps mort
    if tempsMortBoost (tempsMortA):
        return True
    if tempsMortBoost (tempsMortB):
        return True
    return False

def tempsMortAcabat (tempsMort): #simplement per mostrar que ja no es pot tornar a demanar
    if tempsMort == 9:
        return True
    else:
        return False

def jocAcabat (puntsJocA, puntsJocB):
    return (puntsJocA >= 11 or puntsJocB >= 11) and abs(puntsJocA - puntsJocB) >= 2

def partitAcabat (puntsA, puntsB):
    jocsA = 0
    jocsB = 0
    for joc in range (0, 5):
        if puntsA[joc] > puntsB[joc]:
                jocsA +=1
        elif puntsA[joc] < puntsB[joc]:
                jocsB +=1

    return jocsA >= 3 or jocsB >= 3

def clearScreen ():
    #cls
    #print ("\033c")
    clear () #trinket

def puntsPrint (punts, color):
    return colored('{:2d}'.format(punts), color)

def imprimeixHeader ():
    print (colored ("\nTorneig benèfic Família Josep Gual", "cyan"))

# joc from 0 to 4
def imprimeixResultat (grup, guanyador, joc, serveiCount,
        jugA, ptsA, jugB, ptsB, bolloA, bolloB, jugABollos, jugBBollos,
        jugACeleb, jugBCeleb, tempsMortA, tempsMortB, connectatA, connectatB):
    #clearScreen ()

    #serveiCount -1 will not print servei
    serveiAStr = "   "
    serveiBStr = "   "
    if serveiCount >= 0 and serveiCount <= 1:
        serveiAStr = " * "
    elif serveiCount >= 2:
        serveiBStr = " * "

    bolloAStr = ""
    queixaAStr = ""
    bolloBStr = ""
    queixaBStr = ""
    if bolloA:
        bolloAStr = colored("  pilota al canto", "green")
        if jugABollos >= 2:
            #queixaBStr = colored("  per flipar! " + str (jugABollos) + " bollos", "magenta")
            queixaBStr = colored("  ben jugat :( " + str (jugABollos) + " bollos", "magenta")
    if bolloB:
        bolloBStr = colored("  pilota al canto", "green")
        if jugBBollos >= 2:
            #queixaAStr = colored ("  per flipar! " + str (jugBBollos) + " bollos", "magenta")
            queixaAStr = colored ("  bollero! " + str (jugBBollos) + " bollos", "magenta")

    if jugACeleb != "":
        jugACeleb = colored("  " + jugACeleb, "blue")
    if jugBCeleb != "":
        jugBCeleb = colored("  " + jugBCeleb, "blue")

    y = "yellow"
    lg = "light_grey"

    jugA = "- " + jugA
    jugB = "- " + jugB

    maxLength = len(jugA)
    if len(jugB) > maxLength:
        maxLength = len(jugB)

    jugAStr = jugA.ljust (maxLength)
    jugBStr = jugB.ljust (maxLength)

    if connectatA:
        jugAStr = colored(jugAStr, "green")
    if connectatB:
        jugBStr = colored(jugBStr, "green")

    #temps mort
    tmAStr = "   "
    tmBStr = "   "
    if tempsMortEspera (tempsMortA):
        tmAStr = colored(" T ", "red")
    if tempsMortEspera (tempsMortB):
        tmBStr = colored(" T ", "red")
    if tempsMortBoost (tempsMortA):
        tmAStr = colored(" T ", "yellow")
    if tempsMortBoost (tempsMortB):
        tmBStr = colored(" T ", "yellow")
    if tempsMortAcabat (tempsMortA):
        tmAStr = colored(" T ", "light_grey")
    if tempsMortAcabat (tempsMortB):
        tmBStr = colored(" T ", "light_grey")

    #print (colored ("\nGrup " + str(grup) + " " + guanyador, lg))
    print (colored ("\n" + str(grup) + " " + guanyador, lg))
    if joc == 0:
        print (jugAStr + tmAStr + serveiAStr + "[" + puntsPrint(ptsA[0], y) + ", " + puntsPrint(ptsA[1], lg) + ", " + puntsPrint(ptsA[2], lg) + "]" + bolloAStr + queixaAStr + jugACeleb)
        print (jugBStr + tmBStr + serveiBStr + "[" + puntsPrint(ptsB[0], y) + ", " + puntsPrint(ptsB[1], lg) + ", " + puntsPrint(ptsB[2], lg) + "]" + bolloBStr + queixaBStr + jugBCeleb)
    elif joc == 1:
        print (jugAStr + tmAStr + serveiAStr + "[" + puntsPrint(ptsA[0], lg) + ", " + puntsPrint(ptsA[1], y) + ", " + puntsPrint(ptsA[2], lg) + "]" + bolloAStr + queixaAStr + jugACeleb)
        print (jugBStr + tmBStr + serveiBStr + "[" + puntsPrint(ptsB[0], lg) + ", " + puntsPrint(ptsB[1], y) + ", " + puntsPrint(ptsB[2], lg) + "]" + bolloBStr + queixaBStr + jugBCeleb)
    elif joc == 2:
        print (jugAStr + tmAStr + serveiAStr + "[" + puntsPrint(ptsA[0], lg) + ", " + puntsPrint(ptsA[1], lg) + ", " + puntsPrint(ptsA[2], y) + "]" + bolloAStr + queixaAStr + jugACeleb)
        print (jugBStr + tmBStr + serveiBStr + "[" + puntsPrint(ptsB[0], lg) + ", " + puntsPrint(ptsB[1], lg) + ", " + puntsPrint(ptsB[2], y) + "]" + bolloBStr + queixaBStr + jugBCeleb)
    elif joc == 3:
        print (jugAStr + tmAStr + serveiAStr + "[" + puntsPrint(ptsA[0], lg) + ", " + puntsPrint(ptsA[1], lg) + ", " + puntsPrint(ptsA[2], lg) +
                ", " + puntsPrint(ptsA[3], y) + "]" + bolloAStr + queixaAStr + jugACeleb)
        print (jugBStr + tmBStr + serveiBStr + "[" + puntsPrint(ptsB[0], lg) + ", " + puntsPrint(ptsB[1], lg) + ", " + puntsPrint(ptsB[2], lg) +
                ", " + puntsPrint(ptsB[3], y) + "]" + bolloBStr + queixaBStr + jugBCeleb)
    else: #4
        print (jugAStr + tmAStr + serveiAStr + "[" + puntsPrint(ptsA[0], lg) + ", " + puntsPrint(ptsA[1], lg) + ", " + puntsPrint(ptsA[2], lg) + ", " +
                puntsPrint(ptsA[3], lg) + ", " + puntsPrint(ptsA[4], y) + "]" + bolloAStr + queixaAStr + jugACeleb)
        print (jugBStr + tmBStr + serveiBStr + "[" + puntsPrint(ptsB[0], lg) + ", " + puntsPrint(ptsB[1], lg) + ", " + puntsPrint(ptsB[2], lg) + ", " +
                puntsPrint(ptsB[3], lg) + ", " + puntsPrint(ptsB[4], y) + "]" + bolloBStr + queixaBStr + jugBCeleb)

def guanyadorStr (jugA, puntsA, jugB, puntsB):
    jocsGuanyatsA = 0
    jocsGuanyatsB = 0
    for joc in range (0, 5):
        if puntsA[joc] > puntsB[joc]:
            jocsGuanyatsA += 1
        elif puntsA[joc] < puntsB[joc]:
            jocsGuanyatsB += 1

    if jocsGuanyatsA > jocsGuanyatsB:
        return ("- Guanyador/a: " + jugA + " (" + str(jocsGuanyatsA) + " - " + str(jocsGuanyatsB) + ")")
    else:
        return ("- Guanyador/a: " + jugB + " (" + str(jocsGuanyatsB) + " - " + str(jocsGuanyatsA) + ")")


# ---- program start ---->

#llegir els grups i jugadors
#exemple de seguent.txt (sense el símbol de # inicial, Emma i Fèlix estan connectats)
#9:Maria Serres:Enric Manzano
#10:Antoni Sayol:Fèlix Julià_
#11:Emma Flores_:Manuel Carmona
#12:Fernando Huguet:Isidro Infant

file = open("seguent.txt", "r")
partits = file.read().splitlines()
p = 0
for line in partits:
    px = p_l[p] #px es cada partit (per no posar p_l[p] que fa el codi més illegible
    (px['grup'], px['jugA'], px['jugB']) = line.split (':')
    if px['jugA'].endswith ('_'):
        px['connectatA'] = True
        px['jugA'] = px['jugA'][:-1]
    if px['jugB'].endswith ('_'):
        px['connectatB'] = True
        px['jugB'] = px['jugB'][:-1]
    p = p+1

nPartits = p

clearScreen ()
imprimeixHeader ()
for p in range (0, nPartits):
    px = p_l[p]
    imprimeixResultat (px['grup'], "", 0, -1, 
        px['jugA'], px['puntsA'],
        px['jugB'], px['puntsB'],
        False, False, 0, 0, "", "", -1, -1,
        px['connectatA'], px['connectatB'])

print ("\n\n ---- Preparats? (s/n) ----\n\n")
opcio = input ()
if not (opcio == "" or opcio == "s" or opcio == "S" or opcio == "y" or opcio == "Y"):
    sys.exit ()

#decidir el servei inicial. serveiCount { 0: 1st serv of A; 1: 2nd serv of A; 2: 1st serv of B; 3: 2nd serv of B)
for p in range (0, len(p_l)):
    px = p_l[p]
    px['serveiCount'] = 0
    px['serveiIniciJoc'] = "A"
    if not serveiPerA ():
        px['serveiCount'] = 2
        px['serveiIniciJoc'] = "B"

while True:
    #mostra els resultats
    clearScreen ()
    imprimeixHeader ()
    for p in range (0, nPartits):
        px = p_l[p]
        imprimeixResultat (px['grup'], px['guanyador'], px['joc'], px['serveiCount'],
                px['jugA'], px['puntsA'], px['jugB'], px['puntsB'],
                px['jugAPuntBollo'], px['jugBPuntBollo'], px['jugABollos'], px['jugBBollos'],
                px['jugACeleb'], px['jugBCeleb'], px['tempsMortA'], px['tempsMortB'],
                px['connectatA'], px['connectatB'])

    #variables a zero per al proper punt
    for p in range (0, nPartits):
        p_l[p]['jugAPuntBollo'] = False
        p_l[p]['jugBPuntBollo'] = False
        p_l[p]['jugACeleb'] = ""
        p_l[p]['jugBCeleb'] = ""

    #es juga el punt
    time.sleep (tempsPerPunt)
    for p in range (0, nPartits):
        px = p_l[p]
        if not px['partitAcabat'] and not tempsMortEspera2 (px['tempsMortA'], px['tempsMortB']):
            if puntPerA (px['serveiCount'] <= 1,
                    tempsMortBoost (px['tempsMortA']), tempsMortBoost (px['tempsMortB']),
                    px['connectatA'], px['connectatB']):
                px['puntsA'][px['joc']] += 1

                if puntDeBollo ():
                    px['jugAPuntBollo'] = True
                    px['jugABollos'] += 1
                else:
                    celeb = puntDeCelebracio ()
                    if celeb != "":
                        px['jugACeleb'] = celeb
                if (px['tempsMortB'] < 0 and
                        not jocAcabat (px['puntsA'][px['joc']], px['puntsB'][px['joc']]) and
                        demanaTempsMort (px['joc'], px['puntsB'], px['puntsA'])):
                    px['tempsMortB'] = 0
            else:
                px['puntsB'][px['joc']] += 1
                if puntDeBollo ():
                    px['jugBPuntBollo'] = True
                    px['jugBBollos'] += 1
                else:
                    celeb = puntDeCelebracio ()
                    if celeb != "":
                        px['jugBCeleb'] = celeb
                if (px['tempsMortA'] < 0 and
                        not jocAcabat (px['puntsA'][px['joc']], px['puntsB'][px['joc']]) and
                        demanaTempsMort (px['joc'], px['puntsA'], px['puntsB'])):
                    px['tempsMortA'] = 0

        #Augmenta el compatdor de temps mort
        if px['tempsMortA'] >= 0 and px['tempsMortA'] < 9:
            px['tempsMortA'] += 1
        if px['tempsMortB'] >= 0 and px['tempsMortB'] < 9:
            px['tempsMortB'] += 1

    #imprimir resultats, mirar si hi ha guanyador, sino assignar proper servei
    clearScreen ()
    imprimeixHeader ()
    for p in range (0, nPartits):
        px = p_l[p]
        imprimeixResultat (px['grup'], px['guanyador'], px['joc'], px['serveiCount'],
                px['jugA'], px['puntsA'], px['jugB'], px['puntsB'],
                px['jugAPuntBollo'], px['jugBPuntBollo'], px['jugABollos'], px['jugBBollos'],
                px['jugACeleb'], px['jugBCeleb'], px['tempsMortA'], px['tempsMortB'],
                px['connectatA'], px['connectatB'])

        if not px['partitAcabat']:
            if jocAcabat (px['puntsA'][px['joc']], px['puntsB'][px['joc']]):
                if partitAcabat(px['puntsA'], px['puntsB']):
                    px['partitAcabat'] = True
                    px['guanyador'] = guanyadorStr (px['jugA'], px['puntsA'],px['jugB'], px['puntsB'])
                else:
                    px['joc'] += 1
                    if px['joc'] == 5:
                        px['joc'] = 4

                    if px['serveiIniciJoc'] == "A":
                        px['serveiIniciJoc'] = "B"
                    else:
                        px['serveiIniciJoc'] = "A"
            elif not tempsMortEspera2 (px['tempsMortA'], px['tempsMortB']):
                px['serveiCount'] += 1
                if px['serveiCount'] >= 4:
                    px['serveiCount'] = 0

            #afegeix 1 punt si s'ha arribat a 10 per a que canvii el servei a cada punt
            if not tempsMortEspera2 (px['tempsMortA'], px['tempsMortB']):
                if px['puntsA'][px['joc']] >= 10 or px['puntsB'][px['joc']] >= 10:
                    px['serveiCount'] +=1

    #imprimir resultats (per veure el proper servei)
    time.sleep (.5)
    clearScreen ()
    imprimeixHeader ()
    for p in range (0, nPartits):
        px = p_l[p]
        imprimeixResultat (px['grup'], px['guanyador'], px['joc'], px['serveiCount'],
                px['jugA'], px['puntsA'], px['jugB'], px['puntsB'],
                px['jugAPuntBollo'], px['jugBPuntBollo'], px['jugABollos'], px['jugBBollos'],
                px['jugACeleb'], px['jugBCeleb'], px['tempsMortA'], px['tempsMortB'],
                px['connectatA'], px['connectatB'])

    partitsEnJoc = False
    for p in range (0, nPartits):
        if not p_l[p]['partitAcabat']:
            partitsEnJoc = True
    if not partitsEnJoc:
        break

#tots els partits acabats, mostrar resultats
clearScreen ()
imprimeixHeader ()
for p in range (0, nPartits):
    px = p_l[p]
    imprimeixResultat (px['grup'], px['guanyador'], px['joc'], -1,
            px['jugA'], px['puntsA'], px['jugB'], px['puntsB'],
            False, False, 0, 0, "", "",
            px['tempsMortA'], px['tempsMortB'],
            px['connectatA'], px['connectatB'])
